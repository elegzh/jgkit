//
//  Apple.h
//  JGKit
//
//  Created by Jo Guan on 2020/11/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Apple : NSObject

+ (void)GrowUpByAge:(int)age;

@end

NS_ASSUME_NONNULL_END
