//
//  Log.swift
//  TestSwift
//
//  Created by Jo on 2019/6/30.
//  Copyright © 2019 Jo. All rights reserved.
//

import Foundation

public func jgLog<T>(_ msg: T,
            file: NSString = #file,
            line: Int = #line,
            fn: String = #function) {
    #if DEBUG
    let prefix = "\(file.lastPathComponent)_\(line)_\(fn):"
    print(prefix, msg)
    #endif
}


