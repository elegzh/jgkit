//
//  Extention+UIButton.swift
//  Lottery-swift
//
//  Created by Jo Guan on 2020/7/10.
//  Copyright © 2020 jiasheng. All rights reserved.
//

import UIKit

public extension UIButton {
    
    enum JGImagePosition: Int {
        case left = 0   // 图片在左，文字在右，默认
        case right      // 图片在右，文字在左
        case top        // 图片在上，文字在下
        case bottom     // 图片在下，文字在上
    }

    /**
     *  利用UIButton的titleEdgeInsets和imageEdgeInsets来实现文字和图片的自由排列
     *  注意：这个方法需要在设置图片和文字之后才可以调用，且button的大小要大于 图片大小+文字大小+spacing
     *
     *  @param spacing 图片和文字的间隔
     */
    func jg_setImagePosition(position: JGImagePosition, spacing: CGFloat) {
        let imageWidth = imageView!.image!.size.width
        let imageHeight = imageView!.image!.size.height

        titleLabel?.sizeToFit()
        let labelWidth = titleLabel!.frame.size.width
        let labelHeight = titleLabel!.frame.size.height

        let imageOffsetX = (imageWidth + labelWidth) / 2 - imageWidth / 2 // image中心移动的x距离
        let imageOffsetY = imageHeight / 2 + spacing / 2 // image中心移动的y距离
        let labelOffsetX = (imageWidth + labelWidth / 2) - (imageWidth + labelWidth) / 2 // label中心移动的x距离
        let labelOffsetY = labelHeight / 2 + spacing / 2 // label中心移动的y距离

        switch position {
        case .left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing / 2, bottom: 0, right: spacing / 2)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing / 2, bottom: 0, right: -spacing / 2)
        case .right:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: labelWidth + spacing / 2, bottom: 0, right: -(labelWidth + spacing / 2))
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageHeight + spacing / 2), bottom: 0, right: imageHeight + spacing / 2)
        case .top:
            imageEdgeInsets = UIEdgeInsets(top: -imageOffsetY, left: imageOffsetX, bottom: imageOffsetY, right: -imageOffsetX)
            titleEdgeInsets = UIEdgeInsets(top: labelOffsetY, left: -labelOffsetX, bottom: -labelOffsetY, right: labelOffsetX)
        case .bottom:
            imageEdgeInsets = UIEdgeInsets(top: imageOffsetY, left: imageOffsetX, bottom: -imageOffsetY, right: -imageOffsetX)
            titleEdgeInsets = UIEdgeInsets(top: -labelOffsetY, left: -labelOffsetX, bottom: labelOffsetY, right: labelOffsetX)
        }
    }
}
